{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module System.FilePath.FilePather.ByteString(
  fromFilePath
, readFileB
, writeFileB
, appendFileB
, module B
) where

import Control.Exception ( Exception )
import Data.ByteString( ByteString )
import qualified Data.ByteString as B
import System.FilePath.FilePather.ReadFilePath
    ( ReadFilePathT, tryReadFilePath )
import System.IO ( IO )

fromFilePath ::
  Exception e =>
  ReadFilePathT e IO ByteString
fromFilePath =
  tryReadFilePath B.fromFilePath
{-# INLINE fromFilePath #-}

readFileB ::
  Exception e =>
  ReadFilePathT e IO ByteString
readFileB =
  tryReadFilePath B.readFile
{-# INLINE readFileB #-}

writeFileB ::
  Exception e =>
  ByteString
  -> ReadFilePathT e IO ()
writeFileB s =
  tryReadFilePath (`B.writeFile` s)
{-# INLINE writeFileB #-}

appendFileB ::
  Exception e =>
  ByteString
  -> ReadFilePathT e IO ()
appendFileB s =
  tryReadFilePath (`B.appendFile` s)
{-# INLINE appendFileB #-}
