{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module System.FilePath.FilePather.Posix (
  splitExtension
, takeExtension
, replaceExtension
, dropExtension
, addExtension
, hasExtension
, splitExtensions
, dropExtensions
, takeExtensions
, replaceExtensions
, isExtensionOf
, stripExtension
, splitFileName
, takeFileName
, replaceFileName
, dropFileName
, takeBaseName
, replaceBaseName
, takeDirectory
, replaceDirectory
, combine
, splitPath
, joinPath
, splitDirectories
, splitDrive
, joinDrive
, takeDrive
, hasDrive
, dropDrive
, isDrive
, hasTrailingPathSeparator
, addTrailingPathSeparator
, dropTrailingPathSeparator
, normalise
, equalFilePath
, makeRelative
, isRelative
, isAbsolute
, isValid
, makeValid
, module SFP
) where

import Control.Applicative ( Applicative )
import Control.Category((.))
import Data.String( String )
import Data.Bool( Bool )
import Data.Maybe ( Maybe )
import qualified System.FilePath.Posix as FP
import System.FilePath.Posix as SFP( FilePath )
import System.FilePath.FilePather.ReadFilePath
    ( ReadFilePathT, liftReadFilePath )
import System.FilePath.FilePather.ReadFilePaths
    ( ReadFilePathsT, liftReadFilePaths )

splitExtension ::
  Applicative f =>
  ReadFilePathT e f (String, String)
splitExtension =
  liftReadFilePath FP.splitExtension
{-# INLINE splitExtension #-}

takeExtension ::
  Applicative f =>
  ReadFilePathT e f String
takeExtension =
  liftReadFilePath FP.takeExtension
{-# INLINE takeExtension #-}

replaceExtension ::
  Applicative f =>
  String
  -> ReadFilePathT e f FilePath
replaceExtension =
  liftReadFilePath . FP.replaceExtension
{-# INLINE replaceExtension #-}

dropExtension ::
  Applicative f =>
  ReadFilePathT e f FilePath
dropExtension =
  liftReadFilePath FP.dropExtensions
{-# INLINE dropExtension #-}

addExtension ::
  Applicative f =>
  String
  -> ReadFilePathT e f FilePath
addExtension =
  liftReadFilePath . FP.addExtension
{-# INLINE addExtension #-}

hasExtension ::
  Applicative f =>
  ReadFilePathT e f Bool
hasExtension =
  liftReadFilePath FP.hasExtension
{-# INLINE hasExtension #-}

splitExtensions ::
  Applicative f =>
  ReadFilePathT e f (FilePath, String)
splitExtensions =
  liftReadFilePath FP.splitExtensions
{-# INLINE splitExtensions #-}

dropExtensions ::
  Applicative f =>
  ReadFilePathT e f FilePath
dropExtensions =
  liftReadFilePath FP.dropExtensions
{-# INLINE dropExtensions #-}

takeExtensions ::
  Applicative f =>
  ReadFilePathT e f String
takeExtensions =
  liftReadFilePath FP.takeExtensions
{-# INLINE takeExtensions #-}

replaceExtensions ::
  Applicative f =>
  String
  -> ReadFilePathT e f FilePath
replaceExtensions =
  liftReadFilePath . FP.replaceExtensions
{-# INLINE replaceExtensions #-}

isExtensionOf ::
  Applicative f =>
  String
  -> ReadFilePathT e f Bool
isExtensionOf =
  liftReadFilePath . FP.isExtensionOf
{-# INLINE isExtensionOf #-}

stripExtension ::
  Applicative f =>
  String
  -> ReadFilePathT e f (Maybe FilePath)
stripExtension =
  liftReadFilePath . FP.stripExtension
{-# INLINE stripExtension #-}

splitFileName ::
  Applicative f =>
  ReadFilePathT e f (String, String)
splitFileName =
  liftReadFilePath FP.splitFileName
{-# INLINE splitFileName #-}

takeFileName ::
  Applicative f =>
  ReadFilePathT e f String
takeFileName =
  liftReadFilePath FP.takeFileName
{-# INLINE takeFileName #-}

replaceFileName ::
  Applicative f =>
  String
  -> ReadFilePathT e f FilePath
replaceFileName =
  liftReadFilePath . FP.replaceFileName
{-# INLINE replaceFileName #-}

dropFileName ::
  Applicative f =>
  ReadFilePathT e f FilePath
dropFileName =
  liftReadFilePath FP.dropFileName
{-# INLINE dropFileName #-}

takeBaseName ::
  Applicative f =>
  ReadFilePathT e f String
takeBaseName =
  liftReadFilePath FP.takeBaseName
{-# INLINE takeBaseName #-}

replaceBaseName ::
  Applicative f =>
  String ->
  ReadFilePathT e f FilePath
replaceBaseName =
  liftReadFilePath . FP.replaceBaseName
{-# INLINE replaceBaseName #-}

takeDirectory ::
  Applicative f =>
  ReadFilePathT e f FilePath
takeDirectory =
  liftReadFilePath FP.takeDirectory
{-# INLINE takeDirectory #-}

replaceDirectory ::
  Applicative f =>
  String ->
  ReadFilePathT e f FilePath
replaceDirectory =
  liftReadFilePath . FP.replaceDirectory
{-# INLINE replaceDirectory #-}

combine ::
  Applicative f =>
  FilePath
  ->  ReadFilePathT e f FilePath
combine =
  liftReadFilePath . FP.combine
{-# INLINE combine #-}

splitPath ::
  Applicative f =>
  ReadFilePathT e f [FilePath]
splitPath =
  liftReadFilePath FP.splitPath
{-# INLINE splitPath #-}

joinPath ::
  Applicative f =>
  ReadFilePathsT e f FilePath
joinPath =
  liftReadFilePaths FP.joinPath
{-# INLINE joinPath #-}

splitDirectories ::
  Applicative f =>
  ReadFilePathT e f [FilePath]
splitDirectories =
  liftReadFilePath FP.splitDirectories
{-# INLINE splitDirectories #-}

splitDrive ::
  Applicative f =>
  ReadFilePathT e f (FilePath, FilePath)
splitDrive =
  liftReadFilePath FP.splitDrive
{-# INLINE splitDrive #-}

joinDrive ::
  Applicative f =>
  FilePath
  -> ReadFilePathT e f FilePath
joinDrive =
  liftReadFilePath . FP.joinDrive
{-# INLINE joinDrive #-}

takeDrive ::
  Applicative f =>
  ReadFilePathT e f FilePath
takeDrive =
  liftReadFilePath FP.takeDrive
{-# INLINE takeDrive #-}

hasDrive ::
  Applicative f =>
  ReadFilePathT e f Bool
hasDrive =
  liftReadFilePath FP.hasDrive
{-# INLINE hasDrive #-}

dropDrive ::
  Applicative f =>
  ReadFilePathT e f FilePath
dropDrive =
  liftReadFilePath FP.dropDrive
{-# INLINE dropDrive #-}

isDrive ::
  Applicative f =>
  ReadFilePathT e f Bool
isDrive =
  liftReadFilePath FP.isDrive
{-# INLINE isDrive #-}

hasTrailingPathSeparator ::
  Applicative f =>
  ReadFilePathT e f Bool
hasTrailingPathSeparator =
  liftReadFilePath FP.hasTrailingPathSeparator
{-# INLINE hasTrailingPathSeparator #-}

addTrailingPathSeparator ::
  Applicative f =>
  ReadFilePathT e f FilePath
addTrailingPathSeparator =
  liftReadFilePath FP.addTrailingPathSeparator
{-# INLINE addTrailingPathSeparator #-}

dropTrailingPathSeparator ::
  Applicative f =>
  ReadFilePathT e f FilePath
dropTrailingPathSeparator =
  liftReadFilePath FP.dropTrailingPathSeparator
{-# INLINE dropTrailingPathSeparator #-}

normalise ::
  Applicative f =>
  ReadFilePathT e f FilePath
normalise =
  liftReadFilePath FP.normalise
{-# INLINE normalise #-}

equalFilePath ::
  Applicative f =>
  FilePath
  -> ReadFilePathT e f Bool
equalFilePath =
  liftReadFilePath . FP.equalFilePath
{-# INLINE equalFilePath #-}

makeRelative ::
  Applicative f =>
  FilePath
  -> ReadFilePathT e f FilePath
makeRelative =
  liftReadFilePath . FP.makeRelative
{-# INLINE makeRelative #-}

isRelative ::
  Applicative f =>
  ReadFilePathT e f Bool
isRelative =
  liftReadFilePath FP.isRelative
{-# INLINE isRelative #-}

isAbsolute ::
  Applicative f =>
  ReadFilePathT e f Bool
isAbsolute =
  liftReadFilePath FP.isAbsolute
{-# INLINE isAbsolute #-}

isValid ::
  Applicative f =>
  ReadFilePathT e f Bool
isValid =
  liftReadFilePath FP.isValid
{-# INLINE isValid #-}

makeValid ::
  Applicative f =>
  ReadFilePathT e f FilePath
makeValid =
  liftReadFilePath FP.makeValid
{-# INLINE makeValid #-}
