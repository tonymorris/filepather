0.5.6

* Add `traverseFilePath` function
* Add `opticFilePaths` function

0.5.5

* Update `readProcessWithExitCode` function

0.5.4

* add `Process` module

0.5.3

* Re-export some `System.Directory` functions as `ReadFilePathT`

0.5.2

* Add `ReadFilePathsT` to read a list of `FilePath`
* Add `joinPath` value

0.5.1

* Fix bug in `Find` module
* Add function to `Find` module
* `INLINE` definitions

0.5

* Introduce `Either` into filepath reader
* Remove `ToFilePath`
* Remove some functions that do not fit
* Update GHC version requirement >= 9

0.4.1

* Add `System.Directory`
* Other minor improvements

0.4.0

* Rewrite of filepather

0.3.0

* This change log starts
